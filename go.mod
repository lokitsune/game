module gitlab.com/lokitsune/game

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.8.0
)
