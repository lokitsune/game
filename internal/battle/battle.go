/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package battle

import (
	"math/rand"
	"time"

	"github.com/google/uuid"
)

const (
	// Updates per second
	UPS = 60.0

	MaxProgress = 100.0
)

type state uint

const (
	waiting state = iota
	selecting
	preparing
	executing
)

const (
	selectionThreshold = 75.0
)

type battle struct {
	id         string
	combatants []Combatant
	field      *Field

	commands map[string][]Command

	events chan Event
}

func newBattle(combatants []Combatant, width, height int) *battle {
	return &battle{
		id:         uuid.NewString(),
		combatants: combatants,
		field: &Field{
			Width:  width,
			Height: height,
		},
		commands: make(map[string][]Command),
		events:   make(chan Event, 20),
	}
}

func (b *battle) Send(command Command) {
	b.commands[command.Combatant] = append(b.commands[command.Combatant], command)
}

func (b *battle) step(delta float64) {
	var tokens []*Token
	for _, token := range b.field.tokens {
		if !token.paused {
			tokens = append(tokens, token)
		}
	}

	for _, token := range tokens {
		token.Progress += delta * float64(token.combatant.initiative)
		if token.Progress >= selectionThreshold {
			b.changeState(token, selecting)
		}

	}
	for _, token := range b.field.tokens {
		commands := b.commands[token.combatant.id]
		if len(commands) > 0 {
			command := commands[0]
			b.commands[command.Combatant] = commands[1:]
			if token.state == selecting {
				b.changeState(token, preparing)
			}

		}
	}
}

func (b *battle) changeState(t *Token, s state) {
	if t.state == s {
		// token is already in this state, nothing to be done
		return
	}
	t.state = s

	switch s {
	case selecting:
		for _, t1 := range b.field.tokens {
			if t1.combatant.id != t.combatant.id {
				t1.paused = true
			}
		}
	case preparing:
		var unpause = true
		for _, t1 := range b.field.tokens {
			if t1.combatant.id != t.combatant.id && t1.state == selecting {
				unpause = false
			}
		}

		if unpause {
			for _, t1 := range b.field.tokens {
				t1.paused = false
			}
		}
	}

	b.events <- Noop{}
}

func (b battle) Events() <-chan Event {
	return b.events
}

type Field struct {
	Width  int
	Height int
	tokens []*Token
}

/*
   token is a representation of a combatant on the battlefield.
   A token is used to separate the battle-specific state of a combatant, like the position on the battelfield
*/
type Token struct {
	X         int
	Y         int
	combatant *Combatant
	state     state
	Progress  float64
	paused    bool
}

type Combatant struct {
	id         string
	name       string
	initiative int
}

type BattleManager struct {
	battles []*battle
	random  *rand.Rand
}

func NewBattleManager(seed int64) *BattleManager {
	return &BattleManager{
		random: rand.New(rand.NewSource(seed)),
	}
}

func (bm *BattleManager) CreateBattle(combatants []Combatant) error {
	width := 10
	height := 10
	b := newBattle(combatants, width, height)
	bm.battles = append(bm.battles, b)

	xPositions := make(map[int]struct{})
	yPositions := make(map[int]struct{})

	initEvent := BattleInit{}
	initEvent.timestamp = time.Now()
	initEvent.Battlefield = *b.field

	for _, combatant := range combatants {
		x := bm.random.Intn(width-1) + 1
		for _, ok := xPositions[x]; ok; _, ok = xPositions[x] {
			x = bm.random.Intn(width-1) + 1
		}
		xPositions[x] = struct{}{}

		y := bm.random.Intn(height-1) + 1
		for _, ok := yPositions[y]; ok; _, ok = yPositions[y] {
			y = bm.random.Intn(width-1) + 1
		}
		yPositions[y] = struct{}{}

		c := combatant
		token := &Token{
			X:         x,
			Y:         y,
			combatant: &c,
		}
		b.field.tokens = append(b.field.tokens, token)

		initEvent.Tokens = append(initEvent.Tokens, *token)
	}

	b.events <- initEvent

	return nil
}
