/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package battle

import (
	"testing"

	"github.com/google/uuid"
	. "github.com/smartystreets/goconvey/convey"
)

func TestBattleManager(t *testing.T) {

	Convey("Given a new BattleManager", t, func() {
		bm := NewBattleManager(0)

		Convey("Creating a new battle with a slice of combatants", func() {
			c := []Combatant{} //{1, "Foo"}, {2, "Bar"}}
			for i := uint(0); i < 8; i++ {
				j := i
				c = append(c, Combatant{uuid.NewString(), "", int(j + 1)})

			}
			bm.CreateBattle(c)
			Convey("A battle should have been created", func() {
				So(bm.battles, ShouldHaveLength, 1)
				b := bm.battles[0]
				Convey("The battle should not have id 0", func() {
					So(b.id, ShouldNotEqual, 0)
				})

				Convey("The battle should have the same combatants", func() {
					So(b.combatants, ShouldResemble, c)
				})
				Convey("Creating multiple new battles", func() {
					bm.CreateBattle(c)
					bm.CreateBattle(c)
					Convey("Each should have a unique id", func() {
						So(b.id, ShouldNotEqual, bm.battles[1].id)
						So(b.id, ShouldNotEqual, bm.battles[2].id)
						So(bm.battles[1].id, ShouldNotEqual, bm.battles[2].id)
					})
				})

				Convey("A battlefield is created", func() {
					So(b.field.Height, ShouldNotEqual, 0)
					So(b.field.Width, ShouldNotEqual, 0)
				})
				Convey("A 'BattleInit' event must be emitted", func() {
					So(b.Events(), ShouldHaveLength, 1)
					So(<-b.Events(), ShouldHaveSameTypeAs, BattleInit{})
				})

				Convey("Every combatant gains a token", func() {
					So(b.field.tokens, ShouldHaveLength, len(c))
					for i := 0; i < len(c); i++ {
						So(*(b.field.tokens[i].combatant), ShouldBeIn, c)
					}
					Convey("Each token must have a different combatant", func() {
						for i := 0; i < len(c); i++ {
							for j := 0; j < len(c); j++ {
								if i == j {
									continue
								}
								So(b.field.tokens[i].combatant, ShouldNotEqual, b.field.tokens[j].combatant)
							}
						}

					})
					Convey("Every token has a random position", func() {
						for i := 0; i < len(c); i++ {
							So(b.field.tokens[i].X, ShouldBeBetweenOrEqual, 1, b.field.Width-1)
							So(b.field.tokens[i].Y, ShouldBeBetweenOrEqual, 1, b.field.Height-1)
						}

						Convey("The positions must not overlap", func() {
							for i := 0; i < len(c); i++ {
								for j := 0; j < len(c); j++ {
									if i == j {
										continue
									}
									So(b.field.tokens[i].X, ShouldNotEqual, b.field.tokens[j].X)
									So(b.field.tokens[i].Y, ShouldNotEqual, b.field.tokens[j].Y)
								}
							}

						})

					})
					Convey("A token is initialized in the 'waiting' state", func() {
						for i := 0; i < len(c); i++ {
							So(b.field.tokens[i].state, ShouldEqual, waiting)
						}

					})

					Convey("Stepping the battle", func() {
						delta := 1.0 / UPS
						b.step(delta)
						Convey("Should increase the progress of a token by its combatants initiative", func() {
							for i := 0; i < len(c); i++ {
								So(b.field.tokens[i].Progress, ShouldEqual, delta*float64(b.field.tokens[i].combatant.initiative))
							}
						})
					})
					Convey("Stepping the battle until the selectionThreshold is reached", func() {
						// Clear events
						for len(b.Events()) > 0 {
							<-b.Events()
						}
						b.step(selectionThreshold / 2)
						Convey("Every token having reached this threshold must change its state to 'selecting'", func() {
							So(b.field.tokens[0].state, ShouldEqual, waiting)
							for i := 1; i < len(c); i++ {
								So(b.field.tokens[i].state, ShouldEqual, selecting)
							}

							// Clear events
							for len(b.Events()) > 0 {
								<-b.Events()
							}

							Convey("Sending a command to a combatant with a token in the 'selecting' state and stepping the battle should put it into the 'preparing' state", func() {
								b.Send(Command{Combatant: c[1].id})
								b.step(0)
								So(b.field.tokens[1].state, ShouldEqual, preparing)
								Convey("For every token, whose state has been changed to 'preparing', an event is send", func() {
									So(b.Events(), ShouldHaveLength, 1)
								})
								Convey("The other tokens are not unpaused", func() {
									for i := 1; i < len(c); i++ {
										So(b.field.tokens[i].paused, ShouldBeTrue)
									}
								})
								Convey("Sending a command to every combatant", func() {
									for i := 0; i < len(c); i++ {
										b.Send(Command{Combatant: c[i].id})
									}
									b.step(0)
									Convey("Every token is unpaused", func() {
										for i := 0; i < len(c); i++ {
											So(b.field.tokens[i].paused, ShouldBeFalse)
										}
									})

								})

							})
							Convey("Not sending a command to a combatant with a token in the 'selecting' state and stepping the battle should do nothing", func() {
								b.step(0)
								So(b.field.tokens[1].state, ShouldEqual, selecting)
								So(b.Events(), ShouldHaveLength, 0)
							})
							Convey("Sending a command to a combatant with a token in the 'waiting' state and stepping the battle should do nothing", func() {
								b.Send(Command{Combatant: c[0].id})
								b.step(0)
								So(b.field.tokens[0].state, ShouldEqual, waiting)
							})
						})
						Convey("For every token, whose state has been changed to 'selecting', an event is send", func() {
							So(b.Events(), ShouldHaveLength, len(c)-1)
						})
					})
					Convey("Stepping the battle until the selectionThreshold for a single token is reached", func() {
						for i := 0; i < len(c); i++ {
							b.field.tokens[i].paused = false
						}

						b.step(10)
						So(b.field.tokens[len(c)-1].state, ShouldEqual, selecting)
						for i := 0; i < len(c)-1; i++ {
							So(b.field.tokens[i].state, ShouldEqual, waiting)
						}
						Convey("Every other token is paused", func() {
							for i := 0; i < len(c)-1; i++ {
								So(b.field.tokens[i].paused, ShouldBeTrue)
							}
							Convey("Further stepping does not modify the other tokens", func() {
								p := []float64{}
								for i := 0; i < len(c)-1; i++ {
									p = append(p, b.field.tokens[i].Progress)
								}
								b.step(1000)
								for i := 0; i < len(c)-1; i++ {
									So(b.field.tokens[i].Progress, ShouldEqual, p[i])
								}
							})
						})
						Convey("The token is not paused", func() {
							So(b.field.tokens[len(c)-1].paused, ShouldBeFalse)
						})
						Convey("Sending a command to the combatant", func() {
							b.Send(Command{Combatant: c[len(c)-1].id})
							b.step(0)
							So(b.field.tokens[len(c)-1].state, ShouldEqual, preparing)
							Convey("Every other token is unpaused", func() {
								for i := 0; i < len(c)-1; i++ {
									So(b.field.tokens[i].paused, ShouldBeFalse)
								}

							})

						})
					})

				})
			})

		})

	})
}
