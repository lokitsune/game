/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package battle

import "time"

type Event interface {
	Type() EventType
	Timestamp() time.Time
}

type EventType string

type event struct {
	timestamp time.Time
}

func (e event) Timestamp() time.Time {
	return e.timestamp
}

type BattleInit struct {
	event
	Tokens      []Token
	Battlefield Field
}

func (e BattleInit) Type() EventType {
	return "BattleInit"
}

type Noop struct {
	event
}

func (e Noop) Type() EventType {
	return "Noop"
}
