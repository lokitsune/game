/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package server

import (
	"net"
	"os"
	"time"
)

type ChannelConn struct {
	read  <-chan []byte
	write chan<- []byte

	closed bool

	writeTimeout time.Time
	readTimeout  time.Time
}

func NewChannelConn() (*ChannelConn, *ChannelConn) {
	c1 := make(chan []byte, 1)
	c2 := make(chan []byte, 1)
	return &ChannelConn{
			read:  c1,
			write: c2,
		}, &ChannelConn{
			read:  c2,
			write: c1,
		}
}

func (c *ChannelConn) Read(b []byte) (int, error) {
	if c.closed {
		return 0, os.ErrClosed
	}
	timer := time.NewTimer(time.Until(c.readTimeout))

	// If the timeout is 0, no timeout should occur
	if c.readTimeout.Equal(time.Time{}) {
		if !timer.Stop() {
			<-timer.C
		}
	}
	select {
	case v := <-c.read:
		return copy(b, v), nil
	case <-timer.C:
		return 0, os.ErrDeadlineExceeded
	}
}

func (c *ChannelConn) Write(b []byte) (int, error) {
	if c.closed {
		return 0, os.ErrClosed
	}

	timer := time.NewTimer(time.Until(c.writeTimeout))

	// If the timeout is 0, no timeout should occur
	if c.writeTimeout.Equal(time.Time{}) {
		if !timer.Stop() {
			<-timer.C
		}
	}

	select {
	case c.write <- b:
		return len(b), nil
	case <-timer.C:
		return 0, os.ErrDeadlineExceeded
	}
}

func (c *ChannelConn) Close() error {
	c.closed = true
	close(c.write)
	return nil
}

func (c *ChannelConn) LocalAddr() net.Addr  { return &ChannelAddr{} }
func (c *ChannelConn) RemoteAddr() net.Addr { return &ChannelAddr{} }

func (c *ChannelConn) SetDeadline(t time.Time) error {
	c.SetReadDeadline(t)
	c.SetWriteDeadline(t)
	return nil
}

func (c *ChannelConn) SetReadDeadline(t time.Time) error {
	c.readTimeout = t
	return nil
}

func (c *ChannelConn) SetWriteDeadline(t time.Time) error {
	c.writeTimeout = t
	return nil
}

type ChannelAddr struct {
}

func (a *ChannelAddr) Network() string {
	return "channel"
}

func (a *ChannelAddr) String() string {
	return "channel"
}
