/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package server

import (
	"os"
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestChannelConnection(t *testing.T) {

	Convey("Given a new ChannelConnection", t, func() {
		conn, conn2 := NewChannelConn()
		_ = conn2

		Convey("The internal channels should be initialized", func() {
			So(conn.read, ShouldNotBeNil)
			So(conn.write, ShouldNotBeNil)
		})

		Convey("Closing the ChannelConn", func() {
			conn.Close()

			Convey("The internal channel should be closed", func() {
				ok := true

				select {
				case _, ok = <-conn2.read:
				default:
				}
				So(ok, ShouldBeFalse)
			})

			Convey("Reading should return an error", func() {
				_, err := conn.Read([]byte{})
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, os.ErrClosed)
			})

			Convey("Writing should return an error", func() {
				_, err := conn.Write([]byte{})
				So(err, ShouldNotBeNil)
				So(err, ShouldEqual, os.ErrClosed)
			})

		})

		Convey("LocalAddr should return a ChannelAddr", func() {
			addr := conn.LocalAddr()
			So(addr, ShouldHaveSameTypeAs, &ChannelAddr{})
		})

		Convey("RemoteAddr should return a ChannelAddr", func() {
			addr := conn.RemoteAddr()
			So(addr, ShouldHaveSameTypeAs, &ChannelAddr{})
		})

		Convey("Setting a write deadline", func() {
			conn.write <- []byte("Foo")

			Convey("If a timeout occurs", func() {
				conn.SetWriteDeadline(time.Now())
				b, err := conn.Write([]byte("Test"))

				Convey("Write() must return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldEqual, os.ErrDeadlineExceeded)
				})

				Convey("Write() must not write to the channel", func() {
					So(b, ShouldEqual, 0)
				})

			})

			Convey("If no timeout occurs", func() {
				go func() {
					timer := time.NewTimer(time.Millisecond)
					<-timer.C
					<-conn2.read
				}()

				conn.SetWriteDeadline(time.Now().Add(time.Hour))
				b, err := conn.Write([]byte("Test"))

				Convey("Write() must not return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldBeNil)
				})

				Convey("Write() must write to the channel", func() {
					So(b, ShouldEqual, 4)
					So(<-conn2.read, ShouldResemble, []byte("Test"))
				})

			})

			Convey("Passing a zero value should not result in a timeout", func() {
				conn.SetWriteDeadline(time.Time{})
				go func() {
					timer := time.NewTimer(time.Nanosecond)
					<-timer.C
					<-conn2.read
				}()

				b, err := conn.Write([]byte("Test"))

				Convey("Write() must not return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldBeNil)
				})

				Convey("Write() must write to the channel", func() {
					So(b, ShouldEqual, 4)
					So(<-conn2.read, ShouldResemble, []byte("Test"))
				})

			})

		})

		Convey("Setting a read deadline", func() {

			Convey("If a timeout occurs", func() {
				conn.SetReadDeadline(time.Now())
				res := make([]byte, 4)
				b, err := conn.Read(res)

				Convey("Read() must return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldEqual, os.ErrDeadlineExceeded)
				})

				Convey("Read() must not return data", func() {
					So(b, ShouldEqual, 0)
					So(res, ShouldResemble, make([]byte, 4))
				})

			})

			Convey("If no timeout occurs", func() {
				go func() {
					timer := time.NewTimer(time.Millisecond)
					<-timer.C
					conn2.write <- []byte("Test")
				}()

				conn.SetReadDeadline(time.Now().Add(time.Hour))
				res := make([]byte, 4)
				b, err := conn.Read(res)

				Convey("Read() must not return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldNotEqual, os.ErrDeadlineExceeded)
				})

				Convey("Read() must return the data", func() {
					So(b, ShouldEqual, 4)
					So(res, ShouldResemble, []byte("Test"))
				})

			})

			Convey("Passing a zero value should not result in a timeout", func() {
				conn.SetReadDeadline(time.Time{})
				go func() {
					timer := time.NewTimer(time.Nanosecond)
					<-timer.C
					conn2.write <- []byte("Test")
				}()
				conn.SetReadDeadline(time.Now().Add(time.Hour))
				res := make([]byte, 4)
				b, err := conn.Read(res)

				Convey("Read() must not return a os.ErrDeadlineExceeded error", func() {
					So(err, ShouldNotEqual, os.ErrDeadlineExceeded)
				})

				Convey("Read() must return the data", func() {
					So(b, ShouldEqual, 4)
					So(res, ShouldResemble, []byte("Test"))
				})

			})

		})

		Convey("Calling SetDeadline", func() {
			t := time.Now()
			conn.SetDeadline(t)

			Convey("Must set the write deadline", func() {
				So(conn.writeTimeout, ShouldEqual, t)
			})

			Convey("Must set the read deadline", func() {
				So(conn.readTimeout, ShouldEqual, t)
			})

		})

		Convey("Reading from the ChannelConnection", func() {
			conn2.write <- []byte("Test")
			res := make([]byte, 4)
			b, err := conn.Read(res)

			Convey("Must not return an error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Must return the amount of bytes read", func() {
				So(b, ShouldEqual, 4)
			})

			Convey("Must write the result into the parameter", func() {
				So(res, ShouldResemble, []byte("Test"))
			})

		})

		Convey("Writing from the ChannelConnection", func() {
			b, err := conn.Write([]byte("test"))

			Convey("Must not return an error", func() {
				So(err, ShouldBeNil)
			})

			Convey("Must return the amount of bytes written", func() {
				So(b, ShouldEqual, 4)
			})

			Convey("Must write the parameter", func() {
				res := <-conn2.read
				So(res, ShouldResemble, []byte("test"))
			})

		})

	})

}

func TestChannelAddr(t *testing.T) {

	Convey("Given a new ChannelAddr", t, func() {
		addr := ChannelAddr{}

		Convey("Network() should return 'channel'", func() {
			So(addr.Network(), ShouldEqual, "channel")
		})

		Convey("String() should return 'channel'", func() {
			So(addr.String(), ShouldEqual, "channel")
		})

	})

}
