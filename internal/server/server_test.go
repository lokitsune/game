/*
   Copyright © 2021 Lokitsune

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package server

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestServer(t *testing.T) {

	Convey("Creating a new server", t, func() {
		s := Server{}

		Convey("Requesting a channel-based connection", func() {
			ch := s.CreateChannelConn()

			Convey("The server should keep one side of the connection", func() {
				So(s.conns, ShouldHaveLength, 1)
			})

			Convey("The server and the requester must be able to communicate through the connection", func() {
				s.conns[0].Write([]byte("foo"))
				So(<-ch.read, ShouldResemble, []byte("foo"))

				ch.Write([]byte("bar"))
				b := make([]byte, 3)
				s.conns[0].Read(b)
				So(b, ShouldResemble, []byte("bar"))
			})

		})

	})

}
